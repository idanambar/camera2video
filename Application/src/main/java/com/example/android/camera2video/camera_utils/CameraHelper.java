package com.example.android.camera2video.camera_utils;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.util.SizeF;

import static android.hardware.camera2.CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS;
import static android.hardware.camera2.CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE;

/**
 * Created by omer on 06/02/18.
 */

public class CameraHelper {

    public static CameraParams getCameraParams(CameraManager cameraManager, int number_of_width_pixels, int number_of_height_pixels)
    {
        CameraParams cameraParams;
        try {
            for (final String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraId);
                int cOrientation = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (cOrientation == CameraCharacteristics.LENS_FACING_BACK) {
                    double focalLengthMM =  characteristics.get(LENS_INFO_AVAILABLE_FOCAL_LENGTHS)[0];
                    SizeF sensorSize = characteristics.get(SENSOR_INFO_PHYSICAL_SIZE);
                    double widthFocalLengthPixels = focalLengthMM * number_of_width_pixels / sensorSize.getWidth();
                    double heightFocalLengthPixels = focalLengthMM * number_of_height_pixels / sensorSize.getHeight();
                    //double widthFocalLengthPixels = focalLengthMM * number_of_width_pixels / sensorSize.getHeight();
                    //double heightFocalLengthPixels = focalLengthMM * number_of_height_pixels / sensorSize.getWidth();
                    cameraParams = new CameraParams(heightFocalLengthPixels,widthFocalLengthPixels, number_of_width_pixels, number_of_height_pixels);
                    return cameraParams;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
