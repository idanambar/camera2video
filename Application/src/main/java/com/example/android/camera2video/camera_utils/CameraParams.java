package com.example.android.camera2video.camera_utils;

/**
 * Created by omer on 06/02/18.
 */

public class CameraParams {
    /**
     *  Camera parameters used for calculations
     */
    public CameraParams(double heightFocalLengthPixels, double widthFocalLengthPixels, int number_of_width_pixels, int number_of_height_pixels) {
        this.heightFocalLengthPixels = heightFocalLengthPixels;
        this.widthFocalLengthPixels = widthFocalLengthPixels;
        width_pixles = number_of_width_pixels;
        height_pixels = number_of_height_pixels;
    }

    public int getWidth_pixles() {
        return width_pixles;
    }

    public int getHeight_pixels() {
        return height_pixels;
    }

    public double getWidthFocalLengthPixels() {
        return widthFocalLengthPixels;
    }

    public double getHeightFocalLengthPixels() {
        return heightFocalLengthPixels;
    }

    private int width_pixles;
    private int height_pixels;
    private double widthFocalLengthPixels;
    private double heightFocalLengthPixels;
}