package com.example.android.camera2video.sensor_utils;

public class SensorData {
    private float roll, pitch;
    public SensorData(float roll, float pitch)
    {
        this.roll = roll;
        this.pitch = pitch;
    }


    public float getRoll() {
        return roll;
    }

    public float getPitch() {
        return pitch;
    }

}

