package com.example.android.camera2video.sensor_utils;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Surface;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.SENSOR_SERVICE;

public class SensorHandler {
    private Activity activity;
    private SensorEventListener sensorListener;
    private List<OnSensorChangeListener> listeners;
    SensorManager sensorManager;
    WindowManager windowManager;
    @Nullable
    private final Sensor rotationSensor;
    private int lastAccuracy;

    public SensorHandler(Activity activity) {
        windowManager = activity.getWindow().getWindowManager();
        sensorManager = (SensorManager) activity.getSystemService(Activity.SENSOR_SERVICE);
        listeners = new ArrayList<>();
        // Can be null if the sensor hardware is not available
        rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
    }

    public void setOnSensorChangeListener(OnSensorChangeListener l) {
        listeners.add(l);
    }

    private void notifyListeners(SensorData sensorData) {
        for (OnSensorChangeListener listener : listeners) {
            listener.OnSensorChange(sensorData);
        }
    }

    public void stopListening() {
        listeners.clear();
    }

    public void addSensors() {
        sensorListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if (listeners == null) {
                    return;
                }
                if (lastAccuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
                    return;
                }
                if (sensorEvent.sensor == rotationSensor) {
                    SensorData data = updateOrientation(sensorEvent.values);
                    notifyListeners(data);
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
                if (lastAccuracy != accuracy) {
                    lastAccuracy = accuracy;
                }
            }
        };

        sensorManager.registerListener(sensorListener,
                rotationSensor, SensorManager.SENSOR_DELAY_NORMAL);

    }

    private SensorData updateOrientation(float[] rotationVector) {
        float[] rotationMatrix = new float[9];
        SensorManager.getRotationMatrixFromVector(rotationMatrix, rotationVector);

        final int worldAxisForDeviceAxisX;
        final int worldAxisForDeviceAxisY;

        // Remap the axes as if the device screen was the instrument panel,
        // and adjust the rotation matrix for the device orientation.
        switch (windowManager.getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_0:
            default:
                worldAxisForDeviceAxisX = SensorManager.AXIS_X;
                worldAxisForDeviceAxisY = SensorManager.AXIS_Z;
                break;
            case Surface.ROTATION_90:
                worldAxisForDeviceAxisX = SensorManager.AXIS_Z;
                worldAxisForDeviceAxisY = SensorManager.AXIS_MINUS_X;
                break;
            case Surface.ROTATION_180:
                worldAxisForDeviceAxisX = SensorManager.AXIS_MINUS_X;
                worldAxisForDeviceAxisY = SensorManager.AXIS_MINUS_Z;
                break;
            case Surface.ROTATION_270:
                worldAxisForDeviceAxisX = SensorManager.AXIS_MINUS_Z;
                worldAxisForDeviceAxisY = SensorManager.AXIS_X;
                break;
        }

        float[] adjustedRotationMatrix = new float[9];
        SensorManager.remapCoordinateSystem(rotationMatrix, worldAxisForDeviceAxisX,
                worldAxisForDeviceAxisY, adjustedRotationMatrix);

        // Transform rotation matrix into azimuth/pitch/roll
        float[] orientation = new float[3];
        SensorManager.getOrientation(adjustedRotationMatrix, orientation);

        // Convert radians to degrees
        float pitch = orientation[1] * -57;
        float roll = orientation[2] * -57;

        return new SensorData(pitch, roll);
    }


    public interface OnSensorChangeListener{
        void OnSensorChange(SensorData sensorData);
    }
}


